// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use BaseEffect;
use Extractable;
use TimelineElement;
use TrackElement;
use ffi;
use glib;
use glib::StaticType;
use glib::Value;
use glib::object::Downcast;
use glib::object::IsA;
use glib::signal::SignalHandlerId;
use glib::signal::connect;
use glib::translate::*;
use glib_ffi;
use gobject_ffi;
use std::boxed::Box as Box_;
use std::mem;
use std::mem::transmute;
use std::ptr;

glib_wrapper! {
    pub struct Effect(Object<ffi::GESEffect, ffi::GESEffectClass>): BaseEffect, TrackElement, TimelineElement, Extractable;

    match fn {
        get_type => || ffi::ges_effect_get_type(),
    }
}

impl Effect {
    pub fn new(bin_description: &str) -> Effect {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_none(ffi::ges_effect_new(bin_description.to_glib_none().0))
        }
    }
}

pub trait EffectExt {
    fn get_property_bin_description(&self) -> Option<String>;

    fn connect_property_bin_description_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<Effect> + IsA<glib::object::Object>> EffectExt for O {
    fn get_property_bin_description(&self) -> Option<String> {
        unsafe {
            let mut value = Value::from_type(<String as StaticType>::static_type());
            gobject_ffi::g_object_get_property(self.to_glib_none().0, "bin-description".to_glib_none().0, value.to_glib_none_mut().0);
            value.get()
        }
    }

    fn connect_property_bin_description_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<Box_<Fn(&Self) + 'static>> = Box_::new(Box_::new(f));
            connect(self.to_glib_none().0, "notify::bin-description",
                transmute(notify_bin_description_trampoline::<Self> as usize), Box_::into_raw(f) as *mut _)
        }
    }
}

unsafe extern "C" fn notify_bin_description_trampoline<P>(this: *mut ffi::GESEffect, _param_spec: glib_ffi::gpointer, f: glib_ffi::gpointer)
where P: IsA<Effect> {
    let f: &&(Fn(&P) + 'static) = transmute(f);
    f(&Effect::from_glib_borrow(this).downcast_unchecked())
}
